<?php declare(strict_types=1);

/*
 * This file is part of the value-object-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE.md
 */

namespace PEPrograms\ValueObject;

/**
 * Base interface for value objects
 * Currently only a marker interface
 *
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
interface TheInterface
{
}
