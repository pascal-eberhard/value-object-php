<?php declare(strict_types=1);

/*
 * This file is part of the value-object-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE.md
 */

namespace PEPrograms\ValueObject\Simple\Strings;

use PEPrograms\Utils\ClassAndObject\With\GetInstanceSelf;

/**
 * Factory for string data value object
 *
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
class Factory implements GetInstanceSelf\TheInterface
{

    /**
     * @return self
     */
    public static function get()
    {
        return new self();
    }

    /**
     * Create value object with standard data
     *
     * @param string $data
     * @param bool $emptyAllowed
     * @param string $charset Default: Value::DEFAULT_CHARSET
     * @return Value
     * @throws \LogicException
     * @see \PEPrograms\ValueObject\Simple\Strings\Value::DEFAULT_CHARSET
     */
    public function create(string $data, bool $emptyAllowed, string $charset = Value::DEFAULT_CHARSET): Value
    {
        return new Value($data, $emptyAllowed, $charset);
    }

    /**
     * Create value object with default value, is set if $data is empty
     *
     * @param string $data
     * @param bool $emptyAllowed
     * @param string $default Optional, default value if $data is empty
     * @param string $charset Default: Value::DEFAULT_CHARSET
     * @return Value
     * @throws \LogicException
     * @see \PEPrograms\ValueObject\Simple\Strings\Value::DEFAULT_CHARSET
     */
    public function createWithDefault(
        string $data,
        bool $emptyAllowed,
        string $default = '',
        string $charset = Value::DEFAULT_CHARSET
    ): Value {
        return new Value($data, $emptyAllowed, $charset, $default);
    }
}
