<?php declare(strict_types=1);

/*
 * This file is part of the value-object-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE.md
 */

namespace PEPrograms\ValueObject\Simple\Strings;

use PEPrograms\Utils\ClassAndObject\With\ToArray;
use PEPrograms\ValueObject;

/**
 * String data value object
 *
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
class Value implements ToArray\TheInterface, ValueObject\TheInterface
{

    /**
     * Default charset
     *
     * @var string
     */
    const DEFAULT_CHARSET = 'utf8';

    use Field\CharsetTrait;
    use Field\DataTrait;
    use Field\EmptyTrait;
    use Field\LengthTrait;
    use ToArrayTrait;

    /**
     * Constructor
     *
     * @param string $data
     * @param bool $emptyAllowed
     * @param string $charset Default: self::DEFAULT_CHARSET
     * @param string $default Optional, default value if $data is empty
     * @throws \LogicException
     * @see \PEPrograms\ValueObject\Simple\Strings\Value::DEFAULT_CHARSET
     */
    public function __construct(
        string $data,
        bool $emptyAllowed,
        string $charset = self::DEFAULT_CHARSET,
        string $default = ''
    ) {
        if ('' == $data) {
            $data = $default;
        }

        $this->charset = $charset;
        $this->data = $data;
        if ('' == $data) {
            if (!$emptyAllowed) {
                throw new \LogicException('$data must not be empty');
            }

            $this->empty = true;
            $this->length = 0;

            return;
        }

        $this->length = \mb_strlen($this->data, $this->charset);
        if ($this->length < 0) {
            throw new \LogicException('mb_strlen() returns < 0 (' . $this->length . ') for (' . $this->data
                . ')');
        } elseif (!$emptyAllowed && $this->length < 1) {
            throw new \LogicException('$data must not be empty');
        }

        $this->empty = ($this->length < 1);
        if ($this->empty) {
            $this->length = 0;
        }
    }

    /**
     * Check empty or not
     *
     * @param bool $empty
     * @return $this
     * @throws \LogicException
     */
    public function checkEmptyOrNot(bool $empty)
    {
        if ($empty && !$this->empty) {
            throw new \LogicException('String must be empty');
        } elseif (!$empty && $this->empty) {
            throw new \LogicException('String must not be empty');
        }

        return $this;
    }

    /**
     * To string
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->data;
    }
}
