<?php declare(strict_types=1);

/*
 * This file is part of the value-object-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE.md
 */

namespace PEPrograms\ValueObject\Simple\Strings;

/**
 * Value object fields. To use it also for the unit tests
 *
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
trait ToArrayTrait
{

    /**
     * To array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'charset' => $this->charset,
            'data' => $this->data,
            'empty' => $this->empty,
            'length' => $this->length,
        ];
    }
}
