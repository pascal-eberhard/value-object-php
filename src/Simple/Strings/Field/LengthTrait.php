<?php declare(strict_types=1);

/*
 * This file is part of the value-object-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE.md
 */

namespace PEPrograms\ValueObject\Simple\Strings\Field;

/**
 * Value object fields. To use it also for the unit tests
 *
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
trait LengthTrait
{

    /**
     * @var int
     */
    private $length = -1;

    /**
     * Get UTF8 string length
     *
     * @return int
     */
    public function length(): int
    {
        return $this->length;
    }
}
