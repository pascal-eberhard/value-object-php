<?php declare(strict_types=1);

/*
 * This file is part of the value-object-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE.md
 */

namespace PEPrograms\ValueObject\Simple\Strings\Field;

/**
 * Value object fields. To use it also for the unit tests
 *
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
trait EmptyTrait
{

    /**
     * @var bool
     */
    private $empty = true;

    /**
     * Is string empty?
     *
     * @return bool
     */
    public function empty(): bool
    {
        return $this->empty;
    }
}
