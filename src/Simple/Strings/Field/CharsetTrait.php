<?php declare(strict_types=1);

/*
 * This file is part of the value-object-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE.md
 */

namespace PEPrograms\ValueObject\Simple\Strings\Field;

use PEPrograms\ValueObject\Simple\Strings;

/**
 * Value object fields. To use it also for the unit tests
 *
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
trait CharsetTrait
{

    /**
     * @var string
     */
    private $charset = Strings\Value::DEFAULT_CHARSET;

    /**
     * Get charset
     *
     * @return string Default: Value::DEFAULT_CHARSET
     * @see \PEPrograms\ValueObject\Simple\Strings\Value::DEFAULT_CHARSET
     */
    public function charset(): string
    {
        return $this->charset;
    }
}
