<?php declare(strict_types=1);

/*
 * This file is part of the value-object-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE.md
 */

namespace PEPrograms\ValueObject\Tests\Simple\Strings;

use PEPrograms\ValueObject\Simple\Strings as ToTest;
use PHPUnit\Framework\TestCase;

/**
 * @todo Later build the simple test cases automatic based at unit test annotations
 * @covers \PEPrograms\ValueObject\Simple\Strings\Factory
 * @coversDefaultClass \PEPrograms\ValueObject\Simple\Strings\Value
 *
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * Shell: (vendor/bin/phpunit tests/Simple/Strings/TheTest.php)
 * @ \b, else all tests matching "testX*" would be executed
 */
class TheTest extends TestCase
{

    /**
     * @return array, per item:
     * param OutputHelper $expected
     * param StringData $current
     * param string $note About the test case
     * @see self::testDefault()
     */
    public function dataDefault()
    {
        $f = ToTest\Factory::get();
        $german = '"äöüßÄÖÜ" etc., some german special chars';
        $japanese = '"This is japanese" translated to japanese, see (https://www.deepl.com/translator)';
        return [
            // @codingStandardsIgnoreStart
            [new OutputHelper('', true, 0), $f->create('', true), ''],
            [new OutputHelper('abcäöüßABCÄÖÜ', false, 13), $f->create('abcäöüßABCÄÖÜ', false), $german],
            [new OutputHelper('abcäöüßABCÄÖÜ', false, 13), $f->create('abcäöüßABCÄÖÜ', true), $german],
            [new OutputHelper('ÄÖÜacb', false, 6), $f->create('ÄÖÜacb', false), $german],
            [new OutputHelper('ÄÖÜacb', false, 6), $f->create('ÄÖÜacb', true), $german],
            [new OutputHelper('x', false, 1), $f->createWithDefault('', false, 'x'), ''],
            [new OutputHelper('x', false, 1), $f->createWithDefault('', true, 'x'), ''],
            [new OutputHelper('x', false, 1), $f->create('x', false), ''],
            [new OutputHelper('x', false, 1), $f->create('x', true), ''],
            [new OutputHelper('これがジャパニーズ', false, 9), $f->create('これがジャパニーズ', false), $japanese],
            [new OutputHelper('これがジャパニーズ', false, 9), $f->create('これがジャパニーズ', true), $japanese],
            // @codingStandardsIgnoreEnd
        ];
    }

    /**
     * @return array, per item:
     * param string $exception Class path
     * param string $regex Exception message
     * param InputHelper $data
     * @see self::testExceptions()
     */
    public function dataExceptions()
    {
        return [
            // @codingStandardsIgnoreStart
            [\LogicException::class, '/data must not be empty/iu', new InputHelper('', false)],
            // @codingStandardsIgnoreEnd
        ];
    }

    /**
     * @return array, per item:
     * param bool $result Expected is_string() at implicit string cast
     * param StringData $current
     * param string $note About the test case
     * @see self::testToString()
     */
    public function dataToString()
    {
        $f = ToTest\Factory::get();
        return [
            [true, $f->create('', true), 'Empty'],
            [true, $f->create('x', true), true, 'Not empty'],
        ];
    }

    /**
     * @covers ::__construct
     * @covers ::data
     * @dataProvider dataDefault
     *
     * @param OutputHelper $expected
     * @param ToTest\Value $current
     * @param string $note About the test case
     *
     * Shell: (vendor/bin/phpunit tests/Simple/Strings/TheTest.php --filter '/::testDefault\b/')
     * @ \b, else all tests matching "testX*" would be executed
     */
    public function testDefault(OutputHelper $expected, ToTest\Value $current, string $note)
    {
        $this->assertEquals($expected->data(), $current->data(), $note . '. ::data()');
        $this->assertEquals($expected->empty(), $current->empty(), $note . '. ::empty()');
        $this->assertEquals($expected->length(), $current->length(), $note . '. ::length()');

        $this->assertEquals($expected->toArray(), $current->toArray(), $note . '. ::toArray()');
    }

    /**
     * @covers ::__construct
     * @dataProvider dataExceptions
     *
     * @param string $exception Class path
     * @param string $regex Exception message
     * @param InputHelper $data
     *
     * Shell: (vendor/bin/phpunit tests/Simple/Strings/TheTest.php --filter '/::testExceptions\b/')
     * @ \b, else all tests matching "testX*" would be executed
     */
    public function testExceptions(string $exception, string $regex, InputHelper $data)
    {
        $this->expectException($exception);
        $this->expectExceptionMessageRegExp($regex);
        $data->toObject();
    }

    /**
     * @covers ::__construct
     * @covers ::__toString
     * @dataProvider dataToString
     *
     * @param bool $result Expected is_string() at implicit string cast
     * @param ToTest\Value $current
     * @param string $note About the test case
     *
     * Shell: (vendor/bin/phpunit tests/Simple/Strings/TheTest.php --filter '/::testToString\b/')
     * @ \b, else all tests matching "testX*" would be executed
     */
    public function testToString(bool $result, ToTest\Value $current, string $note)
    {
        $this->assertEquals($result, \is_string('' . $current), $note);
    }
}
