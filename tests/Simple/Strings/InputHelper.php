<?php declare(strict_types=1);

/*
 * This file is part of the value-object-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE.md
 */

namespace PEPrograms\ValueObject\Tests\Simple\Strings;

use PEPrograms\Utils\UnitTests;
use PEPrograms\ValueObject;
use PEPrograms\ValueObject\Simple\Strings as ToTest;

/**
 * @todo Later build these classes automatic based at the class to test
 * Value object for Value class constructor parameter
 * For easier test data handling
 * IMPORTANT Keep up to date to Value class constructor signature
 *
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @see \PEPrograms\ValueObject\Simple\Strings\Value
 */
final class InputHelper implements UnitTests\ObjectInputInterface, ValueObject\TheInterface
{
    use ToTest\Field\CharsetTrait;
    use ToTest\Field\DataTrait;

    /**
     * @var string
     */
    private $default = '';

    /**
     * @var bool
     */
    private $emptyAllowed = true;

    /**
     * Constructor
     *
     * @param string $data
     * @param bool $emptyAllowed
     * @param string $charset Default: Value::DEFAULT_CHARSET
     * @param string $default Optional, default value if $data is empty
     * @see \PEPrograms\ValueObject\Simple\Strings\Value::__construct()
     * @see \PEPrograms\ValueObject\Simple\Strings\Value::DEFAULT_CHARSET
     */
    public function __construct(string $data, bool $emptyAllowed, string $charset = '', string $default = '')
    {
        $this->charset = $charset;
        $this->data = $data;
        $this->default = $default;
        $this->emptyAllowed = $emptyAllowed;
    }

    /**
     * @return string
     */
    public function default(): string
    {
        return $this->default;
    }

    /**
     * @return bool
     */
    public function emptyAllowed(): bool
    {
        return $this->emptyAllowed;
    }

    /**
     * Create the related object with the constructor parameter
     *
     * @return ToTest\Value
     */
    public function toObject(): ToTest\Value
    {
        return new ToTest\Value($this->data, $this->emptyAllowed, $this->charset, $this->default);
    }
}
