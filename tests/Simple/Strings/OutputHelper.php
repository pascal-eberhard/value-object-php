<?php declare(strict_types=1);

/*
 * This file is part of the value-object-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE.md
 */

namespace PEPrograms\ValueObject\Tests\Simple\Strings;

use PEPrograms\Utils\UnitTests;
use PEPrograms\ValueObject;
use PEPrograms\ValueObject\Simple\Strings as ToTest;

/**
 * @todo Later build these classes automatic based at the class to test
 * Value object for Value class get methods output
 * For easier test data handling
 * IMPORTANT Keep up to date to Value class output
 *
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @see \PEPrograms\ValueObject\Simple\Strings\Value
 */
final class OutputHelper implements UnitTests\ObjectOutputInterface, ValueObject\TheInterface
{
    use ToTest\Field\CharsetTrait;
    use ToTest\Field\DataTrait;
    use ToTest\Field\EmptyTrait;
    use ToTest\Field\LengthTrait;
    use ToTest\ToArrayTrait;

    /**
     * Constructor
     *
     * @param string $data
     * @param bool $empty
     * @param int $length
     * @param string $charset Default: Value::DEFAULT_CHARSET
     * @see \PEPrograms\ValueObject\Simple\Strings\Value::__construct()
     * @see \PEPrograms\ValueObject\Simple\Strings\Value::DEFAULT_CHARSET
     */
    public function __construct(string $data, bool $empty, int $length, string $charset = '')
    {
        $this->charset = $charset;
        $this->data = $data;
        $this->empty = $empty;
        $this->length = $length;

        if ('' == $this->charset) {
            $this->charset = ToTest\Value::DEFAULT_CHARSET;
        }
    }
}
